﻿namespace Dictionary.GUI
{
    partial class frmTaoTuDien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNgonNgu1 = new System.Windows.Forms.TextBox();
            this.txtNgonNgu2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtHienCo = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnTaoTuDien = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cbbXoaTuDien = new System.Windows.Forms.ComboBox();
            this.btnXoaTuDien = new System.Windows.Forms.Button();
            this.btnThoat = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtNgonNgu1
            // 
            this.txtNgonNgu1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgonNgu1.Location = new System.Drawing.Point(135, 13);
            this.txtNgonNgu1.Name = "txtNgonNgu1";
            this.txtNgonNgu1.Size = new System.Drawing.Size(166, 21);
            this.txtNgonNgu1.TabIndex = 0;
            // 
            // txtNgonNgu2
            // 
            this.txtNgonNgu2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgonNgu2.Location = new System.Drawing.Point(135, 50);
            this.txtNgonNgu2.Name = "txtNgonNgu2";
            this.txtNgonNgu2.Size = new System.Drawing.Size(166, 21);
            this.txtNgonNgu2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ngôn ngữ 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ngôn ngữ 2";
            // 
            // txtHienCo
            // 
            this.txtHienCo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHienCo.Location = new System.Drawing.Point(135, 122);
            this.txtHienCo.Name = "txtHienCo";
            this.txtHienCo.Size = new System.Drawing.Size(166, 170);
            this.txtHienCo.TabIndex = 2;
            this.txtHienCo.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "Từ điển hiện có";
            // 
            // btnTaoTuDien
            // 
            this.btnTaoTuDien.BackColor = System.Drawing.SystemColors.Window;
            this.btnTaoTuDien.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTaoTuDien.Image = global::Dictionary.Properties.Resources.add;
            this.btnTaoTuDien.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTaoTuDien.Location = new System.Drawing.Point(479, 11);
            this.btnTaoTuDien.Name = "btnTaoTuDien";
            this.btnTaoTuDien.Size = new System.Drawing.Size(108, 33);
            this.btnTaoTuDien.TabIndex = 3;
            this.btnTaoTuDien.Text = "Tạo từ điển";
            this.btnTaoTuDien.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTaoTuDien.UseVisualStyleBackColor = false;
            this.btnTaoTuDien.Click += new System.EventHandler(this.btnTaoTuDien_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 15);
            this.label4.TabIndex = 5;
            this.label4.Text = "Chọn từ điển để xóa";
            // 
            // cbbXoaTuDien
            // 
            this.cbbXoaTuDien.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbXoaTuDien.FormattingEnabled = true;
            this.cbbXoaTuDien.Location = new System.Drawing.Point(135, 85);
            this.cbbXoaTuDien.Name = "cbbXoaTuDien";
            this.cbbXoaTuDien.Size = new System.Drawing.Size(166, 23);
            this.cbbXoaTuDien.TabIndex = 6;
            // 
            // btnXoaTuDien
            // 
            this.btnXoaTuDien.BackColor = System.Drawing.SystemColors.Window;
            this.btnXoaTuDien.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaTuDien.Image = global::Dictionary.Properties.Resources.xoa;
            this.btnXoaTuDien.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnXoaTuDien.Location = new System.Drawing.Point(479, 68);
            this.btnXoaTuDien.Name = "btnXoaTuDien";
            this.btnXoaTuDien.Size = new System.Drawing.Size(108, 33);
            this.btnXoaTuDien.TabIndex = 7;
            this.btnXoaTuDien.Text = "Xóa từ điển";
            this.btnXoaTuDien.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnXoaTuDien.UseVisualStyleBackColor = false;
            this.btnXoaTuDien.Click += new System.EventHandler(this.btnXoaTuDien_Click);
            // 
            // btnThoat
            // 
            this.btnThoat.BackColor = System.Drawing.SystemColors.Window;
            this.btnThoat.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoat.Image = global::Dictionary.Properties.Resources.exit;
            this.btnThoat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnThoat.Location = new System.Drawing.Point(479, 122);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(108, 33);
            this.btnThoat.TabIndex = 8;
            this.btnThoat.Text = "Thoát menu";
            this.btnThoat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnThoat.UseVisualStyleBackColor = false;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(311, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(159, 14);
            this.label5.TabIndex = 9;
            this.label5.Text = "Nhập 2 ngôn ngữ: VD: Anh-Viet";
            // 
            // frmTaoTuDien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.ClientSize = new System.Drawing.Size(600, 304);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnThoat);
            this.Controls.Add(this.btnXoaTuDien);
            this.Controls.Add(this.cbbXoaTuDien);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnTaoTuDien);
            this.Controls.Add(this.txtHienCo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNgonNgu2);
            this.Controls.Add(this.txtNgonNgu1);
            this.MaximizeBox = false;
            this.Name = "frmTaoTuDien";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tạo Từ Điển";
            this.Load += new System.EventHandler(this.frmTaoTuDien_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNgonNgu1;
        private System.Windows.Forms.TextBox txtNgonNgu2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox txtHienCo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnTaoTuDien;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbbXoaTuDien;
        private System.Windows.Forms.Button btnXoaTuDien;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Label label5;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dictionary.DAO;
using Dictionary.BUS;
using System.Xml;
using Dictionary.DTO;
using System.Drawing.Printing;

namespace Dictionary
{
    public partial class frmMain : Form
    {
        XmlReader xmlFile; // dung de xu ly file xml
        DataSet ds ; // luu xml vao dataset
        DataView dv; // loc truy van du lieu trong dataset
        String path;
        //bien path duong dan den tu dien (moi tu dien) có 1 duong dan khac nhau.
        //vi du: Anhviet la DulieuTuDien/Anh-Viet.xml

        private ThemTuMoiBUS themTuMoiBUS = new ThemTuMoiBUS();

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            KhoiTaoThamSo();
        }

        #region Khu Vuc Hau Lam Tao Tu Dien, Phuc Hoi...
        void KhoiTaoThamSo()
        {
            string[] filePahts;// mang kieu chuoi luu so luong file xml
            filePahts = Directory.GetFiles("DuLieuTuDien");
            // Directory.GetFiles là hàm tìm số luong file trong duong dan DULIEUTUDIEN

            string DuongDanMacDinh = "";// nguyen doan nay xuong duoi
            char Xet = '\\';
            char Cham = '.';

            int ViTriKiTuXet = filePahts[0].IndexOf(Xet) + 1;
            int ViTriKiTuCham = filePahts[0].IndexOf(Cham);

            int DoDaiChuoiMacDinh = ViTriKiTuCham - ViTriKiTuXet;// xu li chuoi de lay duong dan

            DuongDanMacDinh = filePahts[0].Substring(ViTriKiTuXet, DoDaiChuoiMacDinh);
            // tìm duong dan den file tu dien xml 

            LoadTuDienVaoCombo();

            String DuongDanFileXML = String.Format(@"\\DuLieuTuDien\\{0}.xml", DuongDanMacDinh);
            path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "") + DuongDanFileXML;
            xmlFile = XmlReader.Create(path, new XmlReaderSettings());
            ds = new DataSet();
            ds.ReadXml(xmlFile);
            xmlFile.Close();
            dv = new DataView(ds.Tables[1]);
            //thong ke tong
            thongketong();
            //loai tu thong ke
            lblLoaitu.Text = DuongDanMacDinh;
        }

        void LoadTuDienVaoCombo()
        {
            //Setup data binding
            this.cbLoaiTuDien.DataSource = BUS.LuaChonNgonNguBUS.KiemTraChonNgonNgu(Directory.GetFiles("DuLieuTuDien"));
            this.cbLoaiTuDien.DisplayMember = "Name";
            this.cbLoaiTuDien.ValueMember = "Name";
            this.cbLoaiTuDien.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void btnPhucHoi_Click(object sender, EventArgs e)
        {
            // Sử dụng try catch để bắt lỗi
            try
            {
                if(MessageBox.Show("Dữ liệu sẽ trở về mặc định?","Phục Hồi",MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    String Source = @"DuLieuMacDinh\dictionaryOriginal.xml";
                    String Destination = @"DuLieuTuDien\Anh-Viet.xml";
                    BUS.TaoMacDinhBUS.KiemTraTaoMacDinhBUS(Source, Destination);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void cbLoaiTuDien_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string DuongDanTuDien = cbLoaiTuDien.SelectedValue.ToString();
            String DuongDanFileXML = String.Format("\\DuLieuTuDien\\{0}.xml", DuongDanTuDien);
            path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "") + DuongDanFileXML;
            xmlFile = XmlReader.Create(path, new XmlReaderSettings());
            ds = new DataSet();
            ds.ReadXml(xmlFile);
        
            lblLoaitu.Text = DuongDanTuDien;

            if (ds.Tables.Count > 1)
            {
                dv = new DataView(ds.Tables[1]);
                lblThongke.Text = BUS.ThongKeTongBUS.TKTongBUS(dv);
                btnThongke.Visible = true;
                lblChuY.Text = "";
                txtTienganh.ReadOnly = false;
                btnTra.Visible = true;
                btnIn.Visible = true;
            }
            else
            {
                dv = new DataView(ds.Tables[0]);
                if(dv.Count == 1)
                {
                    lblThongke.Text = "0";
                    lblChuY.Text = "Chú ý: Từ điển rỗng nút thống kê biến mất\n nên không thể thống kê.\n Bạn vui lòng kiểm tra lại từ điển!";
                    txtSoluong.Text = "";
                    txtChucai.Text = "";
                    dgvThongKe.DataSource = null;
                    txtTienganh.ReadOnly = true; 
                }
                btnThongke.Visible = false;
                txtTienganh.Text = "";
                dgvGoiy.DataSource = null;
                txtTiengviet.Text = "";
                txtPhienAm.Text = "";
                txtLoaiTu.Text = "";
                btnTra.Visible = false;
                btnIn.Visible = false;
                MessageBox.Show("Từ điển trống!");
            }
        }

        private void btnTaoTuDien_Click(object sender, EventArgs e)
        {
            try
            {
                GUI.frmTaoTuDien frm = new GUI.frmTaoTuDien();
                frm.Show();
                frm.FormClosed += FormTao_FormClosing;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        private void FormTao_FormClosing(Object sender, FormClosedEventArgs e)
        {
            KhoiTaoThamSo();
        }
        #endregion

        private void btnTra_Click(object sender, EventArgs e)
        {
            try
            {
                    //Goi lai tru tu BUS gan vao txt
                    string TuTimKiem = txtTienganh.Text;
                    TraTuDTO dto = BUS.TraTuBUS.TraTuTiengAnhBUS(TuTimKiem, dv);
                    txtTiengviet.Text = dto.TiengViet;
                    txtLoaiTu.Text = dto.Loai;
                    txtPhienAm.Text = dto.PhienAm;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnThongke_Click(object sender, EventArgs e)
        {
            string tu = txtChucai.Text;
            string sotu = txtSoluong.Text;
            txtSoluong.Text = BUS.ThongKeTuBUS.TKtu(tu, dv, sotu);
            dgvThongKe.DataSource = BUS.ThongKeTuBUS.TKdgv(tu, dv, sotu);
        }

        private void lamRongGiaoDien()
        {
            txtTMTiengAnh.Text = "";
            txtTMTiengViet.Text = "";
            txtTMPhienAm.Text = "";
            txtTMGhiChu.Text = "";
            txtTMTiengAnh.Focus();
        }

        private void btnLuu_Click_1(object sender, EventArgs e)
        {
            // kiểm tra các textbox đã nhập đầy đủ chưa
            if (txtTMTiengAnh.TextLength > 0 && txtTMTiengViet.TextLength > 0 && txtTMPhienAm.TextLength > 0)
            {
                string tiengAnh = txtTMTiengAnh.Text;
                string tiengViet = txtTMTiengViet.Text;
                string phienAm = txtTMPhienAm.Text;
                string loaiTu = comboBoxTMLoaiTu.Text;
                string ghiChu = txtTMGhiChu.Text;

                try
                {
                    // Gọi hàm thêm từ mới bên BUS
                    bool themThanhCong = themTuMoiBUS.themTuMoi(tiengAnh, tiengViet, phienAm, loaiTu, ghiChu, dv, path);
                    if (themThanhCong)
                    {
                        // Thêm thành công làm rỗng giao diện
                        lamRongGiaoDien();
                    }
                    else
                    {
                        // Thêm không thành công hiện thị MessageBox báo lổi
                        MessageBox.Show("Thêm không thành công");
                    }
                }
                catch (Exception E)
                {
                    // Hiển thị MessageBox báo lổi Exception
                    MessageBox.Show(E.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                // Hiển thị MessageBox báo chưa nhập đủ thông tin
                MessageBox.Show("Cảnh Báo!", "Vui lòng nhập đầy đủ thông tin.");
            }
        }

        // Xử lí tìm file để thêm vào database
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = "CSV File (*.csv)|*.csv";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                DialogResult dialogResult = MessageBox.Show("Bạn có muốn thêm thừ mới từ file này không", "Thông Báo", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    int soLuongTuDaThem = themTuMoiBUS.napTuTapTin(openFileDialog.FileName, dv, path);
                    MessageBox.Show("Đã nhập thành công " + soLuongTuDaThem + " từ");
                }
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            lamRongGiaoDien();
        }

        private void txtTienganh_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (dv != null)
                {
                    if (e.KeyValue == 222 || (e.KeyValue >= 65 && e.KeyValue <= 122) || e.KeyValue == 8 || e.KeyValue == 32 || e.KeyValue == 13 || e.KeyValue == 37 || e.KeyValue == 39 || e.KeyValue == 189)
                    {
                        if (txtTienganh.Text != "")
                        {
                            if (GoiYBUS.XuLyTuTimTrenTxtTiengAnhBUS(txtTienganh.Text, dv) == false)
                            {
                                txtTienganh.Text = "";
                            }
                            dgvGoiy.DataSource = dv;
                            dgvGoiy.Columns["Nghia"].Visible = false;
                            dgvGoiy.Columns["PhienAm"].Visible = false;
                        }
                    }
                    else
                    {
                        DialogResult dialogResult = MessageBox.Show("Từ cần tìm không chứa số ký tự đặc biệt! Mời bạn nhập lại!", "Thông Báo", MessageBoxButtons.OK);
                        txtTienganh.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void dgvGoiy_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            // And you respective cell's value
            string a = dgvGoiy.SelectedCells[0].Value.ToString();
        }

        private void dgvGoiy_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = dgvGoiy.CurrentRow.Index;//chi muc dong hien tai duoc chon 

            string TuTim = dgvGoiy.Rows[rowindex].Cells["id"].Value.ToString();
            string Loai = dgvGoiy.Rows[rowindex].Cells["Loai"].Value.ToString();
            string Nghia = dgvGoiy.Rows[rowindex].Cells["Nghia"].Value.ToString();

            GoiYBUS.XuLyGoiYBUS(TuTim, Loai, Nghia, dv);
            txtTiengviet.Text = dv[0]["Nghia"].ToString(); // gán vào txt
            txtLoaiTu.Text = dv[0]["Loai"].ToString();
            txtPhienAm.Text = dv[0]["PhienAm"].ToString();
            txtTienganh.Text = dv[0]["id"].ToString();
        }
	void LoadDuLieuTheoLoaiTuDien(string DuongDan)
        {
            //LoadTuDienVaoCombo();

            String DuongDanFileXML = DuongDan;
            path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "") + DuongDanFileXML;
            xmlFile = XmlReader.Create(path, new XmlReaderSettings());
            ds = new DataSet();
            ds.ReadXml(xmlFile);
            dv = new DataView(ds.Tables[1]);
        }
        private void cbLoaiTuDien_SelectedIndexChanged(object sender, EventArgs e)
        {
            //KhoiTaoThamSo();
        }

        private void tpTracuu_Click(object sender, EventArgs e)
        {

        }

        private void tpThongke_Click(object sender, EventArgs e)
        {

        }
        private void thongketong()
        {
            //gọi Bus kiểm tra:
            lblThongke.Text = BUS.ThongKeTongBUS.TKTongBUS(dv);
        }

        private void txtChucai_KeyUp(object sender, KeyEventArgs e)
        {   //gọi biến cho bus:
            int chu = e.KeyValue;// giá trị từ nhập trong bảng ascii
            string chunhapvao = txtChucai.Text;// từ nhập
            int dodai = txtChucai.Text.Length;// độ dài từ nhập
            //gọi Bus kiểm tra:
             txtChucai.Text = BUS.ThongKeTuBUS.TxtChucai(chu, chunhapvao, dodai);
        }
        private void btnThoat_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn thoát?", "Thông báo", MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void DocumentToPrint_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string pageData = "Tiếng Anh: " + txtTienganh.Text + Environment.NewLine;
            pageData = pageData + "Tiếng Việt: " + txtTiengviet.Text + Environment.NewLine;
            pageData = pageData + "Loại từ: " + txtLoaiTu.Text + Environment.NewLine;
            pageData = pageData + "Phiên âm: " + txtPhienAm.Text + Environment.NewLine;

            StringReader reader = new StringReader(pageData);
            float YPosition = 0;
            float LeftMargin = e.MarginBounds.Left;
            float TopMargin = e.MarginBounds.Top;
            string Line = null;
            Font PrintFont = new Font(FontFamily.GenericSerif, 15, FontStyle.Bold);
            SolidBrush PrintBrush = new SolidBrush(Color.Green);

            int LineNumber = 0;
            while ((Line = reader.ReadLine()) != null)
            {
                YPosition = TopMargin + (LineNumber * PrintFont.GetHeight(e.Graphics));
                e.Graphics.DrawString(Line, PrintFont, PrintBrush, LeftMargin, YPosition, new StringFormat());
                LineNumber++;
            }
           
            PrintBrush.Dispose();
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
            PrintDocument documentToPrint = new PrintDocument();
            printDialog.Document = documentToPrint;
            if (printDialog.ShowDialog() == DialogResult.OK)
            {
                documentToPrint.PrintPage += new PrintPageEventHandler(DocumentToPrint_PrintPage);
                documentToPrint.Print();
            }
        }
        
    }
}

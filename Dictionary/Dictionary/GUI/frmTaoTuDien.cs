﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace Dictionary.GUI
{
    public partial class frmTaoTuDien : Form
    {
        string[] filePahts;
        public frmTaoTuDien()
        {
            InitializeComponent();
        }

        private void btnTaoTuDien_Click(object sender, EventArgs e)
        {
            string NgonNgu1 = txtNgonNgu1.Text.Trim().ToString();
            string NgonNgu2 = txtNgonNgu2.Text.Trim().ToString();

            BUS.TaoTuDienBUS.KiemTraTaoTuDien(NgonNgu1, NgonNgu2,filePahts);
            ReSet();
        }

        private void frmTaoTuDien_Load(object sender, EventArgs e)
        {
            ReSet();
            
        }
        void ReSet()
        {
            LoadTuDienVaoCombo();
            filePahts = Directory.GetFiles("DuLieuTuDien");

            int Dem = 0;
            string TuDienHienCo = "";
            if (filePahts.Length > 0)
            {
                //
                while (Dem < filePahts.Length)
                {
                    string DuongDanMacDinh = "";
                    char Xet = '\\';
                    char Cham = '.';

                    int ViTriKiTuXet = filePahts[Dem].IndexOf(Xet) + 1;
                    int ViTriKiTuCham = filePahts[Dem].IndexOf(Cham);

                    int DoDaiChuoiMacDinh = ViTriKiTuCham - ViTriKiTuXet;

                    DuongDanMacDinh = filePahts[Dem].Substring(ViTriKiTuXet, DoDaiChuoiMacDinh);

                    //
                    TuDienHienCo += DuongDanMacDinh + "\n";
                    Dem++;
                }
            }
            txtHienCo.Text = TuDienHienCo;
        }

        void LoadTuDienVaoCombo()
        {
            //Setup data binding
            this.cbbXoaTuDien.DataSource = BUS.LuaChonNgonNguBUS.KiemTraChonNgonNgu(Directory.GetFiles("DuLieuTuDien"));
            this.cbbXoaTuDien.DisplayMember = "Name";
            this.cbbXoaTuDien.ValueMember = "Name";
            this.cbbXoaTuDien.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void btnXoaTuDien_Click(object sender, EventArgs e)
        {
            try
            {
                string TuDienCanXoa = cbbXoaTuDien.SelectedValue.ToString();
                BUS.XoaBUS.XoaTuDienBUS(TuDienCanXoa);
                ReSet();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn thoát?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                ReSet();
                this.Close();
            }
        }

        private void frmTaoTuDien_FormClosed(object sender, FormClosedEventArgs e)
        {
            ReSet();
        }
    }
}

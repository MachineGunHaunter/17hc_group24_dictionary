﻿namespace Dictionary
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.tpTaotudienmoi = new System.Windows.Forms.TabPage();
            this.btnTaoTuDien = new System.Windows.Forms.Button();
            this.btnPhucHoi = new System.Windows.Forms.Button();
            this.tpThemtumoi = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.comboBoxTMLoaiTu = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnLuu = new System.Windows.Forms.Button();
            this.txtTMGhiChu = new System.Windows.Forms.TextBox();
            this.txtTMTiengViet = new System.Windows.Forms.TextBox();
            this.txtTMPhienAm = new System.Windows.Forms.TextBox();
            this.txtTMTiengAnh = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.progressBarReadFile = new System.Windows.Forms.ProgressBar();
            this.btnSelectFile = new System.Windows.Forms.Button();
            this.tpThongke = new System.Windows.Forms.TabPage();
            this.lblDanhsachtu = new System.Windows.Forms.Label();
            this.lblLoaitu = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblChuY = new System.Windows.Forms.Label();
            this.dgvThongKe = new System.Windows.Forms.DataGridView();
            this.lblThongke = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnThongke = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSoluong = new System.Windows.Forms.TextBox();
            this.txtChucai = new System.Windows.Forms.TextBox();
            this.tpTracuu = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.cbLoaiTuDien = new System.Windows.Forms.ComboBox();
            this.txtPhienAm = new System.Windows.Forms.TextBox();
            this.txtLoaiTu = new System.Windows.Forms.TextBox();
            this.dgvGoiy = new System.Windows.Forms.DataGridView();
            this.txtTiengviet = new System.Windows.Forms.RichTextBox();
            this.txtTienganh = new System.Windows.Forms.TextBox();
            this.btnTra = new System.Windows.Forms.Button();
            this.btnThoat = new System.Windows.Forms.Button();
            this.btnIn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpTaotudienmoi.SuspendLayout();
            this.tpThemtumoi.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tpThongke.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvThongKe)).BeginInit();
            this.tpTracuu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGoiy)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tpTaotudienmoi
            // 
            this.tpTaotudienmoi.BackColor = System.Drawing.Color.SkyBlue;
            this.tpTaotudienmoi.Controls.Add(this.btnTaoTuDien);
            this.tpTaotudienmoi.Controls.Add(this.btnPhucHoi);
            this.tpTaotudienmoi.Location = new System.Drawing.Point(4, 22);
            this.tpTaotudienmoi.Name = "tpTaotudienmoi";
            this.tpTaotudienmoi.Size = new System.Drawing.Size(776, 535);
            this.tpTaotudienmoi.TabIndex = 2;
            this.tpTaotudienmoi.Text = "Tạo từ điển mới";
            // 
            // btnTaoTuDien
            // 
            this.btnTaoTuDien.Location = new System.Drawing.Point(89, 11);
            this.btnTaoTuDien.Name = "btnTaoTuDien";
            this.btnTaoTuDien.Size = new System.Drawing.Size(75, 23);
            this.btnTaoTuDien.TabIndex = 3;
            this.btnTaoTuDien.Text = "Tạo từ điển mới";
            this.btnTaoTuDien.UseVisualStyleBackColor = true;
            this.btnTaoTuDien.Click += new System.EventHandler(this.btnTaoTuDien_Click);
            // 
            // btnPhucHoi
            // 
            this.btnPhucHoi.Location = new System.Drawing.Point(8, 11);
            this.btnPhucHoi.Name = "btnPhucHoi";
            this.btnPhucHoi.Size = new System.Drawing.Size(75, 23);
            this.btnPhucHoi.TabIndex = 0;
            this.btnPhucHoi.Text = "Phục Hồi";
            this.btnPhucHoi.UseVisualStyleBackColor = true;
            this.btnPhucHoi.Click += new System.EventHandler(this.btnPhucHoi_Click);
            // 
            // tpThemtumoi
            // 
            this.tpThemtumoi.BackColor = System.Drawing.Color.SkyBlue;
            this.tpThemtumoi.Controls.Add(this.tabControl2);
            this.tpThemtumoi.Location = new System.Drawing.Point(4, 22);
            this.tpThemtumoi.Name = "tpThemtumoi";
            this.tpThemtumoi.Padding = new System.Windows.Forms.Padding(3);
            this.tpThemtumoi.Size = new System.Drawing.Size(776, 535);
            this.tpThemtumoi.TabIndex = 1;
            this.tpThemtumoi.Text = "Thêm từ mới ";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Location = new System.Drawing.Point(73, 38);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(621, 430);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.comboBoxTMLoaiTu);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.btnDelete);
            this.tabPage1.Controls.Add(this.btnLuu);
            this.tabPage1.Controls.Add(this.txtTMGhiChu);
            this.tabPage1.Controls.Add(this.txtTMTiengViet);
            this.tabPage1.Controls.Add(this.txtTMPhienAm);
            this.tabPage1.Controls.Add(this.txtTMTiengAnh);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(613, 404);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Thêm Từ Mới";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // comboBoxTMLoaiTu
            // 
            this.comboBoxTMLoaiTu.FormattingEnabled = true;
            this.comboBoxTMLoaiTu.Items.AddRange(new object[] {
            "Danh Từ",
            "Động Từ",
            "Tính Từ"});
            this.comboBoxTMLoaiTu.Location = new System.Drawing.Point(172, 137);
            this.comboBoxTMLoaiTu.Name = "comboBoxTMLoaiTu";
            this.comboBoxTMLoaiTu.Size = new System.Drawing.Size(164, 21);
            this.comboBoxTMLoaiTu.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(81, 140);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Loại Từ:";
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.SystemColors.Window;
            this.btnDelete.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = global::Dictionary.Properties.Resources.xoa;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(457, 343);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(65, 31);
            this.btnDelete.TabIndex = 21;
            this.btnDelete.Text = "Hủy";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.BackColor = System.Drawing.SystemColors.Window;
            this.btnLuu.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Image = global::Dictionary.Properties.Resources.save;
            this.btnLuu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLuu.Location = new System.Drawing.Point(359, 343);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(75, 31);
            this.btnLuu.TabIndex = 20;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLuu.UseVisualStyleBackColor = false;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click_1);
            // 
            // txtTMGhiChu
            // 
            this.txtTMGhiChu.Location = new System.Drawing.Point(172, 252);
            this.txtTMGhiChu.Multiline = true;
            this.txtTMGhiChu.Name = "txtTMGhiChu";
            this.txtTMGhiChu.Size = new System.Drawing.Size(360, 68);
            this.txtTMGhiChu.TabIndex = 19;
            // 
            // txtTMTiengViet
            // 
            this.txtTMTiengViet.Location = new System.Drawing.Point(172, 193);
            this.txtTMTiengViet.Name = "txtTMTiengViet";
            this.txtTMTiengViet.Size = new System.Drawing.Size(360, 20);
            this.txtTMTiengViet.TabIndex = 18;
            // 
            // txtTMPhienAm
            // 
            this.txtTMPhienAm.Location = new System.Drawing.Point(172, 79);
            this.txtTMPhienAm.Name = "txtTMPhienAm";
            this.txtTMPhienAm.Size = new System.Drawing.Size(360, 20);
            this.txtTMPhienAm.TabIndex = 17;
            // 
            // txtTMTiengAnh
            // 
            this.txtTMTiengAnh.Location = new System.Drawing.Point(172, 25);
            this.txtTMTiengAnh.Name = "txtTMTiengAnh";
            this.txtTMTiengAnh.Size = new System.Drawing.Size(360, 20);
            this.txtTMTiengAnh.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(81, 255);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Ghi Chú : ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(81, 196);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Tiếng Việt :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(84, 82);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "Phiên Âm :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(84, 32);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Tiếng Anh :";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.progressBarReadFile);
            this.tabPage2.Controls.Add(this.btnSelectFile);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(613, 404);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Thêm Từ Mới Từ File";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // progressBarReadFile
            // 
            this.progressBarReadFile.Location = new System.Drawing.Point(121, 147);
            this.progressBarReadFile.MarqueeAnimationSpeed = 1000;
            this.progressBarReadFile.Maximum = 100000;
            this.progressBarReadFile.Name = "progressBarReadFile";
            this.progressBarReadFile.Size = new System.Drawing.Size(371, 11);
            this.progressBarReadFile.Step = 1;
            this.progressBarReadFile.TabIndex = 5;
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Location = new System.Drawing.Point(246, 208);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(95, 34);
            this.btnSelectFile.TabIndex = 4;
            this.btnSelectFile.Text = "Chọn File";
            this.btnSelectFile.UseVisualStyleBackColor = true;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // tpThongke
            // 
            this.tpThongke.BackColor = System.Drawing.Color.SkyBlue;
            this.tpThongke.Controls.Add(this.lblDanhsachtu);
            this.tpThongke.Controls.Add(this.lblLoaitu);
            this.tpThongke.Controls.Add(this.label18);
            this.tpThongke.Controls.Add(this.label17);
            this.tpThongke.Controls.Add(this.lblChuY);
            this.tpThongke.Controls.Add(this.dgvThongKe);
            this.tpThongke.Controls.Add(this.lblThongke);
            this.tpThongke.Controls.Add(this.label15);
            this.tpThongke.Controls.Add(this.btnThongke);
            this.tpThongke.Controls.Add(this.label5);
            this.tpThongke.Controls.Add(this.label6);
            this.tpThongke.Controls.Add(this.label7);
            this.tpThongke.Controls.Add(this.label4);
            this.tpThongke.Controls.Add(this.txtSoluong);
            this.tpThongke.Controls.Add(this.txtChucai);
            this.tpThongke.Location = new System.Drawing.Point(4, 22);
            this.tpThongke.Name = "tpThongke";
            this.tpThongke.Padding = new System.Windows.Forms.Padding(3);
            this.tpThongke.Size = new System.Drawing.Size(776, 535);
            this.tpThongke.TabIndex = 0;
            this.tpThongke.Text = "Thống Kê";
            this.tpThongke.Click += new System.EventHandler(this.tpThongke_Click);
            // 
            // lblDanhsachtu
            // 
            this.lblDanhsachtu.AutoSize = true;
            this.lblDanhsachtu.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDanhsachtu.Location = new System.Drawing.Point(329, 86);
            this.lblDanhsachtu.Name = "lblDanhsachtu";
            this.lblDanhsachtu.Size = new System.Drawing.Size(95, 16);
            this.lblDanhsachtu.TabIndex = 14;
            this.lblDanhsachtu.Text = "Danh sách từ:";
            // 
            // lblLoaitu
            // 
            this.lblLoaitu.AutoSize = true;
            this.lblLoaitu.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoaitu.ForeColor = System.Drawing.Color.Black;
            this.lblLoaitu.Location = new System.Drawing.Point(183, 346);
            this.lblLoaitu.Name = "lblLoaitu";
            this.lblLoaitu.Size = new System.Drawing.Size(61, 22);
            this.lblLoaitu.TabIndex = 13;
            this.lblLoaitu.Text = "loaitu";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(9, 412);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(304, 19);
            this.label18.TabIndex = 12;
            this.label18.Text = "Chú ý: Thay đổi loại từ điển ở Tra Cứu Từ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(8, 346);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(137, 22);
            this.label17.TabIndex = 11;
            this.label17.Text = "Loại từ điển : ";
            // 
            // lblChuY
            // 
            this.lblChuY.AutoSize = true;
            this.lblChuY.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChuY.ForeColor = System.Drawing.Color.Red;
            this.lblChuY.Location = new System.Drawing.Point(16, 189);
            this.lblChuY.Name = "lblChuY";
            this.lblChuY.Size = new System.Drawing.Size(0, 19);
            this.lblChuY.TabIndex = 9;
            // 
            // dgvThongKe
            // 
            this.dgvThongKe.BackgroundColor = System.Drawing.Color.White;
            this.dgvThongKe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvThongKe.Location = new System.Drawing.Point(332, 111);
            this.dgvThongKe.Name = "dgvThongKe";
            this.dgvThongKe.Size = new System.Drawing.Size(415, 335);
            this.dgvThongKe.TabIndex = 8;
            // 
            // lblThongke
            // 
            this.lblThongke.AutoSize = true;
            this.lblThongke.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThongke.Location = new System.Drawing.Point(233, 302);
            this.lblThongke.Name = "lblThongke";
            this.lblThongke.Size = new System.Drawing.Size(0, 22);
            this.lblThongke.TabIndex = 7;
            this.lblThongke.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(133, 93);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 14);
            this.label15.TabIndex = 6;
            this.label15.Text = "Thống Kê";
            // 
            // btnThongke
            // 
            this.btnThongke.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThongke.Image = global::Dictionary.Properties.Resources.NutThongKe;
            this.btnThongke.Location = new System.Drawing.Point(124, 111);
            this.btnThongke.Name = "btnThongke";
            this.btnThongke.Size = new System.Drawing.Size(80, 37);
            this.btnThongke.TabIndex = 2;
            this.btnThongke.UseVisualStyleBackColor = true;
            this.btnThongke.Click += new System.EventHandler(this.btnThongke_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(222, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 16);
            this.label5.TabIndex = 1;
            this.label5.Text = "Số lượng từ :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 300);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(188, 22);
            this.label6.TabIndex = 1;
            this.label6.Text = "Tổng số từ vựng  : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(283, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(161, 32);
            this.label7.TabIndex = 1;
            this.label7.Text = "THỐNG KÊ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "Chữ cái đầu :";
            // 
            // txtSoluong
            // 
            this.txtSoluong.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoluong.Location = new System.Drawing.Point(225, 111);
            this.txtSoluong.Multiline = true;
            this.txtSoluong.Name = "txtSoluong";
            this.txtSoluong.Size = new System.Drawing.Size(89, 37);
            this.txtSoluong.TabIndex = 0;
            this.txtSoluong.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtChucai
            // 
            this.txtChucai.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChucai.Location = new System.Drawing.Point(12, 111);
            this.txtChucai.Multiline = true;
            this.txtChucai.Name = "txtChucai";
            this.txtChucai.Size = new System.Drawing.Size(96, 37);
            this.txtChucai.TabIndex = 0;
            this.txtChucai.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtChucai.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtChucai_KeyUp);
            // 
            // tpTracuu
            // 
            this.tpTracuu.BackColor = System.Drawing.Color.SkyBlue;
            this.tpTracuu.Controls.Add(this.label16);
            this.tpTracuu.Controls.Add(this.cbLoaiTuDien);
            this.tpTracuu.Controls.Add(this.txtPhienAm);
            this.tpTracuu.Controls.Add(this.txtLoaiTu);
            this.tpTracuu.Controls.Add(this.dgvGoiy);
            this.tpTracuu.Controls.Add(this.txtTiengviet);
            this.tpTracuu.Controls.Add(this.txtTienganh);
            this.tpTracuu.Controls.Add(this.btnTra);
            this.tpTracuu.Controls.Add(this.btnThoat);
            this.tpTracuu.Controls.Add(this.btnIn);
            this.tpTracuu.Controls.Add(this.label3);
            this.tpTracuu.Controls.Add(this.label14);
            this.tpTracuu.Controls.Add(this.label13);
            this.tpTracuu.Controls.Add(this.label2);
            this.tpTracuu.Controls.Add(this.label1);
            this.tpTracuu.Location = new System.Drawing.Point(4, 22);
            this.tpTracuu.Name = "tpTracuu";
            this.tpTracuu.Size = new System.Drawing.Size(776, 535);
            this.tpTracuu.TabIndex = 3;
            this.tpTracuu.Text = "Tra Cứu Từ";
            this.tpTracuu.Click += new System.EventHandler(this.tpTracuu_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 12.75F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(10, 27);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(103, 19);
            this.label16.TabIndex = 9;
            this.label16.Text = "Loại từ điển";
            // 
            // cbLoaiTuDien
            // 
            this.cbLoaiTuDien.Font = new System.Drawing.Font("Arial", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLoaiTuDien.FormattingEnabled = true;
            this.cbLoaiTuDien.Location = new System.Drawing.Point(127, 25);
            this.cbLoaiTuDien.Name = "cbLoaiTuDien";
            this.cbLoaiTuDien.Size = new System.Drawing.Size(121, 27);
            this.cbLoaiTuDien.TabIndex = 8;
            this.cbLoaiTuDien.SelectionChangeCommitted += new System.EventHandler(this.cbLoaiTuDien_SelectionChangeCommitted);
            // 
            // txtPhienAm
            // 
            this.txtPhienAm.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhienAm.Location = new System.Drawing.Point(441, 410);
            this.txtPhienAm.Name = "txtPhienAm";
            this.txtPhienAm.Size = new System.Drawing.Size(228, 25);
            this.txtPhienAm.TabIndex = 7;
            // 
            // txtLoaiTu
            // 
            this.txtLoaiTu.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoaiTu.Location = new System.Drawing.Point(441, 384);
            this.txtLoaiTu.Name = "txtLoaiTu";
            this.txtLoaiTu.Size = new System.Drawing.Size(228, 25);
            this.txtLoaiTu.TabIndex = 7;
            // 
            // dgvGoiy
            // 
            this.dgvGoiy.AllowUserToDeleteRows = false;
            this.dgvGoiy.AllowUserToResizeColumns = false;
            this.dgvGoiy.AllowUserToResizeRows = false;
            this.dgvGoiy.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvGoiy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGoiy.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGoiy.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvGoiy.GridColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvGoiy.Location = new System.Drawing.Point(14, 222);
            this.dgvGoiy.Name = "dgvGoiy";
            this.dgvGoiy.ReadOnly = true;
            this.dgvGoiy.RowHeadersVisible = false;
            this.dgvGoiy.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGoiy.Size = new System.Drawing.Size(206, 206);
            this.dgvGoiy.TabIndex = 0;
            this.dgvGoiy.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGoiy_CellClick);
            this.dgvGoiy.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvGoiy_CellMouseClick);
            // 
            // txtTiengviet
            // 
            this.txtTiengviet.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTiengviet.Location = new System.Drawing.Point(343, 100);
            this.txtTiengviet.Name = "txtTiengviet";
            this.txtTiengviet.Size = new System.Drawing.Size(425, 260);
            this.txtTiengviet.TabIndex = 3;
            this.txtTiengviet.Text = "";
            // 
            // txtTienganh
            // 
            this.txtTienganh.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienganh.Location = new System.Drawing.Point(14, 100);
            this.txtTienganh.Multiline = true;
            this.txtTienganh.Name = "txtTienganh";
            this.txtTienganh.Size = new System.Drawing.Size(206, 49);
            this.txtTienganh.TabIndex = 5;
            this.txtTienganh.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTienganh_KeyUp);
            // 
            // btnTra
            // 
            this.btnTra.FlatAppearance.BorderSize = 0;
            this.btnTra.Image = ((System.Drawing.Image)(resources.GetObject("btnTra.Image")));
            this.btnTra.Location = new System.Drawing.Point(241, 100);
            this.btnTra.Name = "btnTra";
            this.btnTra.Size = new System.Drawing.Size(85, 36);
            this.btnTra.TabIndex = 2;
            this.btnTra.UseVisualStyleBackColor = true;
            this.btnTra.Click += new System.EventHandler(this.btnTra_Click);
            // 
            // btnThoat
            // 
            this.btnThoat.BackColor = System.Drawing.SystemColors.Window;
            this.btnThoat.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoat.Image = global::Dictionary.Properties.Resources.exit;
            this.btnThoat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnThoat.Location = new System.Drawing.Point(605, 455);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(75, 40);
            this.btnThoat.TabIndex = 2;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnThoat.UseVisualStyleBackColor = false;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btnIn
            // 
            this.btnIn.BackColor = System.Drawing.SystemColors.Window;
            this.btnIn.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIn.Image = global::Dictionary.Properties.Resources._in;
            this.btnIn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIn.Location = new System.Drawing.Point(685, 455);
            this.btnIn.Name = "btnIn";
            this.btnIn.Size = new System.Drawing.Size(82, 40);
            this.btnIn.TabIndex = 2;
            this.btnIn.Text = "In Từ";
            this.btnIn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIn.UseVisualStyleBackColor = false;
            this.btnIn.Click += new System.EventHandler(this.btnIn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Gợi Ý";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label14.Location = new System.Drawing.Point(339, 409);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 19);
            this.label14.TabIndex = 0;
            this.label14.Text = "Phiên Âm";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(339, 385);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 19);
            this.label13.TabIndex = 0;
            this.label13.Text = "Loại từ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(339, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tiếng Việt";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tiếng Anh";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpTracuu);
            this.tabControl1.Controls.Add(this.tpThongke);
            this.tabControl1.Controls.Add(this.tpThemtumoi);
            this.tabControl1.Controls.Add(this.tpTaotudienmoi);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(784, 561);
            this.tabControl1.TabIndex = 5;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dictionary";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.tpTaotudienmoi.ResumeLayout(false);
            this.tpThemtumoi.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tpThongke.ResumeLayout(false);
            this.tpThongke.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvThongKe)).EndInit();
            this.tpTracuu.ResumeLayout(false);
            this.tpTracuu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGoiy)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tpTaotudienmoi;
        private System.Windows.Forms.Button btnPhucHoi;
        private System.Windows.Forms.TabPage tpThemtumoi;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tpThongke;
        private System.Windows.Forms.Button btnThongke;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSoluong;
        private System.Windows.Forms.TextBox txtChucai;
        private System.Windows.Forms.TabPage tpTracuu;
        private System.Windows.Forms.DataGridView dgvGoiy;
        private System.Windows.Forms.RichTextBox txtTiengviet;
        private System.Windows.Forms.TextBox txtTienganh;
        private System.Windows.Forms.Button btnTra;
        private System.Windows.Forms.Button btnIn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ComboBox comboBoxTMLoaiTu;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnLuu;
        private System.Windows.Forms.TextBox txtTMGhiChu;
        private System.Windows.Forms.TextBox txtTMTiengViet;
        private System.Windows.Forms.TextBox txtTMPhienAm;
        private System.Windows.Forms.TextBox txtTMTiengAnh;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.ProgressBar progressBarReadFile;
        private System.Windows.Forms.Button btnSelectFile;
        private System.Windows.Forms.TextBox txtPhienAm;
        private System.Windows.Forms.TextBox txtLoaiTu;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbLoaiTuDien;
        private System.Windows.Forms.Button btnTaoTuDien;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblThongke;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.DataGridView dgvThongKe;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblChuY;
        private System.Windows.Forms.Label lblLoaitu;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblDanhsachtu;
    }
}


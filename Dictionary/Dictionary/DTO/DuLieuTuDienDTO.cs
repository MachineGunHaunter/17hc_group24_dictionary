﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dictionary.DTO
{
    class DuLieuTuDienDTO
    {
        private string _TiengAnh;
        private string _TiengViet;

        public string TiengAnh
        {
            get
            {
                return _TiengAnh;
            }

            set
            {
                _TiengAnh = value;
            }
        }

        public string TiengViet
        {
            get
            {
                return _TiengViet;
            }

            set
            {
                _TiengViet = value;
            }
        }
    }
}

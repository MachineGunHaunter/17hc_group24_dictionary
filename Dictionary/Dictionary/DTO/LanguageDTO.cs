﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dictionary.DTO
{
    public class LanguageDTO
    {
        public string Name { get; set; }
    }
}

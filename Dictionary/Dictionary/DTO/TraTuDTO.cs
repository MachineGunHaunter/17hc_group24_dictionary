﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dictionary.DTO
{
    public class TraTuDTO
    {
        public int ID { get; set; }
        public string TiengAnh { get; set; }
        public string TiengViet { get; set; }
        public string PhienAm { get; set; }
        public string Loai { get; set; }
        public string GhiChu { get; set; }
    }
}

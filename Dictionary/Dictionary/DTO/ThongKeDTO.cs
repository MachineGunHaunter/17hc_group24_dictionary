﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dictionary.DTO
{
    class ThongKeDTO
    {
        private string _ChuCai;
        private string _SoTu;

        public string ChuCai
        {
            get
            {
                return _ChuCai;
            }

            set
            {
                _ChuCai = value;
            }
        }

        public string SoTu
        {
            get
            {
                return _SoTu;
            }

            set
            {
                _SoTu = value;
            }
        }
    }
}

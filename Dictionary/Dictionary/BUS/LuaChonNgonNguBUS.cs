﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dictionary.DTO;
using Dictionary.DAO;
using System.Windows.Forms;

namespace Dictionary.BUS
{
    class LuaChonNgonNguBUS
    {
        internal LuaChonNgonNguDAO LuaChonNgonNguDAO
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public static List<LanguageDTO> KiemTraChonNgonNgu(string[] filePahts)
        {
            try
            {
                if (filePahts.Length > 0)
                {
                    return DAO.LuaChonNgonNguDAO.ChonNgonNguDAO(filePahts);

                }
                else
                {
                    MessageBox.Show("Không có dữ liệu");
                    return null;
                    
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dictionary.DAO;
using System.Windows.Forms;

namespace Dictionary.BUS
{
    class XoaBUS
    {
        internal XoaDAO XoaDAO
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public static void XoaTuDienBUS(string TuDienCanXoa)
        {
            String HoiXoa = String.Format("Bạn có muốn xóa từ điển {0} không?", TuDienCanXoa);
            if (MessageBox.Show(HoiXoa, "Xóa từ điển", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (TuDienCanXoa == "Anh-Viet")
                {
                    MessageBox.Show("Anh-Viet là từ điển mặc định. Vui lòng xóa từ điển khác!");
                }
                else
                {
                    DAO.XoaDAO.XoaTuDienDAO(TuDienCanXoa);
                    String Xoa = String.Format("Xóa từ điển {0} thành công!", TuDienCanXoa);
                    MessageBox.Show(Xoa);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dictionary.BUS
{
    class ThongKeTongBUS
    {
        public static string TKTongBUS(DataView dv)
        {
            try
            {
                string kq;
                kq = DAO.ThongKeTongDAO.TKTongDAO(dv);// gọi Dao để thực hiện lệnh
                return kq;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return null;
            }

        }
    }
}

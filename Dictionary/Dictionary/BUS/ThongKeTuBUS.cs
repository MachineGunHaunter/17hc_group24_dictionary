﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dictionary.DAO;
using System.Data;

namespace Dictionary.BUS
{
    class ThongKeTuBUS
    {
        public static string TxtChucai(int chu, string chunhap, int dodaitu)
        {
            try
            {
                if (dodaitu == 1 && (chu >= 65 && chu <= 90) || chu == 13 || chu == 8)
                {
                }
                else
                {
                    chunhap = "";
                    MessageBox.Show("Bạn phải nhập 1 chữ cái thường. Vui lòng nhập lại !", "Thông báo!",
                        MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());// nếu sai thì báo lỗi hệ thống(cách mặc định)
            }
            return chunhap;
        }
        public static string TKtu(string tu, DataView dv,string sotu) 
        {
            try
            {
                if (tu == "")
                {
                    MessageBox.Show("Bạn phải nhập từ để thống kê. Vui lòng nhập lại!", "Thông báo",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                    return "";
                }
                string kq;
                kq = ThongKeTuDAO.TKtu(tu,dv, sotu);
                return kq;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        public static DataView TKdgv(string tu, DataView dv, string sotu)
        {
            try
            {

                if (tu == "")
                {
                    return null;
                }
                DataView kq;
                kq = ThongKeTuDAO.TKdgv(tu, dv);
                return kq;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
    }
}
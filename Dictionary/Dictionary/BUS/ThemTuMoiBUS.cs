﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Linq;
using System.Text;

namespace Dictionary.BUS
{
    class ThemTuMoiBUS
    {
        private DAO.ThemTuMoiDAO themTuMoiDAO = new DAO.ThemTuMoiDAO("asbnas");

        internal DAO.ThemTuMoiDAO ThemTuMoiDAO
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public bool themTuMoi(string tiengAnh, string tiengViet, string phienAm, string loaiTu, string ghiChu, DataView dv, string path)
        {
            // Kiểm tra từ cần thêm đã tồn tại trong database chưa
            if (themTuMoiDAO.KiemTraTonTai(tiengAnh, loaiTu, dv))
            {
                throw new Exception("Từ " + tiengAnh + " đã tồn tại");
            }
            return themTuMoiDAO.ThemTuMoi(tiengAnh, tiengViet, phienAm, loaiTu, ghiChu, dv, path);
        }

        public int napTuTapTin(string tapTinCSV, DataView dv, string path)
        {
            // Khai báo biến số lượng từ đã thêm (mặc định là 0)
            int count = 0;

            using (var streamReader = new StreamReader(tapTinCSV))
            {
                // Duyệt qua các dòng trong tập tin
                while (!streamReader.EndOfStream)
                {
                    // Cắt dòng bởi đấu phẩy
                    string[] splits = streamReader.ReadLine().Split(',');
                    bool columnValid = splits.Length >= 5;
                    if (columnValid)
                    {
                        // Lấy nội dung từ mảng các dữ liệu cắt dòng
                        string tiengAnh = splits[0];
                        string tiengViet = splits[1];
                        string phienAm = splits[2];
                        string loaiTu = splits[3];
                        string ghiChu = splits[4];

                        // Nếu từ không tồn tại trong cơ sở dữ liệu thì thêm vào
                        if (!themTuMoiDAO.KiemTraTonTai(tiengAnh, loaiTu, dv))
                        {
                            if (themTuMoiDAO.ThemTuMoi(tiengAnh, tiengViet, phienAm, loaiTu, ghiChu, dv, path))
                            {
                                // Nếu thêm thành công vào cơ sở dữ liệu thì tăng biến đếm từ thêm được lên
                                count++;
                            };
                        }
                    }

                    
                }
            }

            return count;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dictionary.DAO;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Data;
using Dictionary.DTO;

namespace Dictionary.BUS
{
    public static class TraTuBUS
    {
        internal static TraTuDAO TraTuDAO
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public static TraTuDTO TraTuTiengAnhBUS(string TuCanTim, DataView dv)
        {
            TraTuDTO ketqua = new TraTuDTO();
            //Kiem tra tu can tim khac rong
            if (TuCanTim != "")
            {
                //Kiem tra tu can tim trong co so du lieu
                if (TraTuDAO.TimTuDAO(TuCanTim, dv).TiengViet == null)
                {
                    MessageBox.Show("Không có trog từ điển! Nhập lại từ cần tra!");
                    return ketqua;
                }
                else
                {
                    //Xuat tu can tim
                    return TraTuDAO.TimTuDAO(TuCanTim, dv);
                }
            }
            else
            {
                MessageBox.Show("Không có trog từ điển! Nhập lại từ cần tra!");
                return ketqua;
            }
        }

    }
}

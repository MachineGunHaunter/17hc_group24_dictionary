﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dictionary.BUS
{
    class TaoTuDienBUS
    {
        internal DAO.TaoTuDienDAO TaoTuDienDAO
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public static void KiemTraTaoTuDien(string NgonNgu1, string NgonNgu2, string[] filePahts)
        {
            try
            {
                bool KiemTra = true;
                if (NgonNgu1 != "" && NgonNgu2 != "")
                {
                    string TenTuDienMoi = NgonNgu1 + "-" + NgonNgu2;

                    for(int i = 0; i < filePahts.Length; i++)
                    {
                        string DuongDanMacDinh = "";
                        char Xet = '\\';
                        char Cham = '.';

                        int ViTriKiTuXet = filePahts[i].IndexOf(Xet) + 1;
                        int ViTriKiTuCham = filePahts[i].IndexOf(Cham);

                        int DoDaiChuoiMacDinh = ViTriKiTuCham - ViTriKiTuXet;

                        DuongDanMacDinh = filePahts[i].Substring(ViTriKiTuXet, DoDaiChuoiMacDinh);

                        if (TenTuDienMoi == DuongDanMacDinh)
                        {
                            KiemTra = false;
                        }
                    }
                }
                else if (NgonNgu1 == "" || NgonNgu2 == "")
                {
                    MessageBox.Show("Tên từ điển không được rổng!");
                    return;
                }

                if(KiemTra == true)
                {
                    DAO.TaoTuDienDAO.ThucThiTaoTuDien(NgonNgu1, NgonNgu2);
                }
                else
                {
                    MessageBox.Show("Tên từ điển đã tồn tại! Vui lòng nhập tên khác!");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

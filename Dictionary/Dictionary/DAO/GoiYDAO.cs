﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;

namespace Dictionary.DAO
{
    class GoiYDAO
    {
        public static bool XuLyTuTimTrenTxtTiengAnh(string TuCanTim, DataView dv)
        {
            string TuTim = TuCanTim;
            string CatTu = " ";
            string DauNhay = "'";
            int DODAI = TuTim.IndexOf(DauNhay);
            //Tim vi tri cua "'"
            if (TuTim.IndexOf(DauNhay) > -1) // co dau nhay
            {

                //Cat chuoi den dau '
                TuTim = TuTim.Substring(0, DODAI);
                if (TuCanTim.Length > DODAI + 1)// co tu sau dau nhay
                {
                    CatTu = TuCanTim.Substring(DODAI + 1, (TuCanTim.Length - DODAI) - 1);
                    if (CatTu == "" || CatTu =="'")//
                    {
                        DialogResult dialogResult = MessageBox.Show("Từ cần tìm không chứa số ký tự đặc biệt! Mời bạn nhập lại!", "Thông Báo", MessageBoxButtons.OK);
                        return false;
                    }
                }
            }
            //
            if (TuCanTim.IndexOf(DauNhay) > -1 && TuCanTim.Length <= DODAI + 1)// khong co tu sau nhay
            {
                string Loc = string.Format("id like '{0}%'", TuTim);
                dv.RowFilter = Loc;
                return true;
            }
            else if (TuCanTim.Length > DODAI + 1 && DODAI != -1)// co tu sau nhay
            {
                string Loc = string.Format("id like '{0}%' and id like'%{1}%'", TuTim, CatTu);
                dv.RowFilter = Loc;
                return true;
            }
            else// khong co dau nhay
            {
                dv.RowFilter = "id Like '" + TuTim + "%'";
                return true;
            }
        }
        public static void XuLyTuGoiY(string TuTim, string Loai, string Nghia, DataView dv)
        {
            string DauNhay = "'";
            //Tim vi tri cua "'"
            if (TuTim.IndexOf(DauNhay) > -1)
            {
                int DODAI = TuTim.IndexOf(DauNhay);
                //Cat chuoi den dau '
                TuTim = TuTim.Substring(0, DODAI);
            }

            if (Loai.Length > 20)
            {
                Loai = Loai.Substring(0, 20);
            }
            if (Nghia.Length > 20)
            {
                Nghia = Nghia.Substring(0, 20);
            }
            string LocTu = String.Format("id like '{0}%' and loai like '{1}%' and nghia like '{2}%'", TuTim, Loai, Nghia);
            dv.RowFilter = LocTu;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows.Forms;

namespace Dictionary.DAO
{
    class TaoTuDienDAO
    {
        public static void ThucThiTaoTuDien(string NgonNgu1, string NgonNgu2)
        {
            String createXML = String.Format(@"DuLieuTuDien\\{0}-{1}.xml", NgonNgu1, NgonNgu2);

            String LoaiTuDien = String.Format("{0}-{1}", NgonNgu1, NgonNgu2);

            XmlWriter xmlWriter = XmlWriter.Create(createXML);

            xmlWriter.WriteStartDocument();

            xmlWriter.WriteStartElement("TuDien");
            xmlWriter.WriteAttributeString("Loai", LoaiTuDien);
            xmlWriter.WriteEndElement();

            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
            MessageBox.Show("Tạo từ điển thành công");
        }
    }
}

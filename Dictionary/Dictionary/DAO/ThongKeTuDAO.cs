﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Dictionary.DAO
{
    class ThongKeTuDAO
    {
        public static string TKtu(string tu, DataView dv, string soluongtu )
        {
            dv.RowFilter = String.Format("id like '{0}%'", tu);
            soluongtu = dv.Count.ToString();
            return soluongtu;
        }
        public static DataView TKdgv(string tu, DataView dv)
        {
            dv.RowFilter = String.Format("id like '{0}%'", tu);
            return dv;
            //  dgvThongKe.Columns.Remove("id");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dictionary.DTO;
using System.Data.OleDb;
using System.Xml;
using System.Data;
using System.IO;

namespace Dictionary.DAO
{
    class TraTuDAO 
    {

        public static TraTuDTO TimTuDAO(string TuTimKiem, DataView dv)
        {
            TraTuDTO ketqua = new TraTuDTO();
            //Loc ket qua tu DataView
            dv.RowFilter = String.Format("id like '{0}'", TuTimKiem);
            //Lay ket qua tu DataView
            if (dv.Count > 0)
            {
                ketqua.TiengViet = dv[0]["Nghia"].ToString();
                ketqua.PhienAm = dv[0]["PhienAm"].ToString();
                ketqua.Loai = dv[0]["Loai"].ToString();
                return ketqua;
            }
            else
            {
                return ketqua;
            }
        }
    }
}

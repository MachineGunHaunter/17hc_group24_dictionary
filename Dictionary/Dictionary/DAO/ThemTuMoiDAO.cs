﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;
using System.Xml;
using System.Xml.Linq;

namespace Dictionary.DAO
{
    class ThemTuMoiDAO
    {
        private XmlWriter writer;

        public ThemTuMoiDAO(string pathXml)
        {
           // writer = XmlWriter.Create(pathXml);
        }

        public bool KiemTraTonTai(string tiengAnh, string loaiTu, DataView dv)
        {
            try
            {
                foreach (DataRowView rowView in dv)
                {
                    DataRow row = rowView.Row;
                    if (row["id"].Equals(tiengAnh) && row["Loai"].Equals(loaiTu))
                    {
                        return true;
                    }
                }
            }
            catch (Exception E)
            {
                MessageBox.Show(E.Message);
                return false;
            }
            return false; ;
        }

        public bool ThemTuMoi(string tiengAnh, string tiengViet, string phienAm, string loaiTu, string ghiChu, DataView dv, string path)
        {
            // Push new row into dv
            DataRowView newRow = dv.AddNew();
            newRow["id"] = tiengAnh;
            newRow["Nghia"] = tiengViet + Environment.NewLine + ghiChu;
            newRow["PhienAm"] = phienAm;
            newRow["Loai"] = loaiTu;
            newRow.EndEdit();
            try
            {
                XDocument tuVungXML = XDocument.Load(path);
                XElement newTu = new XElement("Tu",
                new XElement("Nghia", tiengViet + Environment.NewLine + ghiChu),
                new XElement("PhienAm", phienAm),
                new XElement("Loai", loaiTu));
                newTu.SetAttributeValue("id", tiengAnh);
                tuVungXML.Element("TuDien").Add(newTu);
                tuVungXML.Save(path);
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }
    }
}

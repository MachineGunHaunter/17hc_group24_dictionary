﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dictionary.DTO;
using System.IO;

namespace Dictionary.DAO
{
    class LuaChonNgonNguDAO
    {
        static public List<LanguageDTO> ChonNgonNguDAO(string[] filePahts)
        {
            

            var dataSource = new List<LanguageDTO>();
            int Dem = 0;
            if (filePahts.Length > 0)
            {
                //
                while (Dem < filePahts.Length)
                {
                    string DuongDanMacDinh = "";
                    char Xet = '\\';
                    char Cham = '.';

                    int ViTriKiTuXet = filePahts[Dem].IndexOf(Xet) + 1;
                    int ViTriKiTuCham = filePahts[Dem].IndexOf(Cham);

                    int DoDaiChuoiMacDinh = ViTriKiTuCham - ViTriKiTuXet;

                    DuongDanMacDinh = filePahts[Dem].Substring(ViTriKiTuXet, DoDaiChuoiMacDinh);

                    //
                    dataSource.Add(new LanguageDTO() { Name = DuongDanMacDinh });
                    Dem++;
                }
            }
            return dataSource;
        }
    }
}

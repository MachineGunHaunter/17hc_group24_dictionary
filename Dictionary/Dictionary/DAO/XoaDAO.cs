﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dictionary.DAO
{
    class XoaDAO
    {
        public static void XoaTuDienDAO(string TuDienCanXoa)
        {
            String DuongDanFileXoa = String.Format(@"DULIEUTUDIEN\{0}.xml", TuDienCanXoa);
            System.IO.File.Delete(DuongDanFileXoa);
        }
    }
}
